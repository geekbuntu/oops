<?php
//start bottom up -> as apposed to top down - in plain english
/*
vehicle (singular)

*perspectives:
    what can it do? (who is allowed to use it?)
    what does it have (that's worth mentioning)?
    what is it's purpose?



wheeledVehicle
    -properties:
        name
        model
        weight
        cost
        passenger_capacity
        engine_specs
        length(ft in)
        year
        model
        maxspeed
        fueltype
        wheelsize //child specifics are not needed globally

    -methods:
        startEngine
        moveForward
        adjustThrottle
        moveBackwards
        turnLeft
        turnRight
        turnLightsOn





waterVehicle
    length
    engine_type
    engine_specs
    craft_type
    fuel_capacity



*/


?>